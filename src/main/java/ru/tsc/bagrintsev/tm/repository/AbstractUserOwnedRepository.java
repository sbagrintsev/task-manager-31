package ru.tsc.bagrintsev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.repository.IUserOwnedRepository;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @NotNull
    @Override
    public M add(
            @NotNull final String userId,
            @NotNull final M record
    ) {
        record.setUserId(userId);
        return add(record);
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) {
        return records.stream()
                .filter(record -> userId.equals(record.getUserId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<M> findAll(
            @NotNull final String userId,
            @NotNull final Comparator<M> comparator
    ) {
        return records.stream()
                .filter(record -> userId.equals(record.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public M findOneByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) {
        return findAll(userId).get(index);
    }

    @NotNull
    @Override
    public M findOneById(
            final @NotNull String userId,
            final @NotNull String id
    ) throws AbstractException {
        return records.stream()
                .filter(record -> userId.equals(record.getUserId()) && id.equals(record.getId()))
                .findFirst()
                .orElseThrow(ModelNotFoundException::new);
    }

    @Override
    public boolean existsById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        findOneById(userId, id);
        return true;
    }

    @NotNull
    @Override
    public M remove(
            @NotNull final String userId,
            @NotNull final M record
    ) {
        if (userId.equals(record.getUserId())) {
            records.remove(record);
        }
        return record;
    }

    @NotNull
    @Override
    public M removeByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) {
        @NotNull final Optional<M> record = Optional.of(findOneByIndex(userId, index));
        record.ifPresent(records::remove);
        return record.get();
    }

    @NotNull
    @Override
    public M removeById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        @NotNull final Optional<M> record = Optional.of(findOneById(userId, id));
        record.ifPresent(records::remove);
        return record.get();
    }

    @Override
    public long totalCount(@NotNull final String userId) {
        return records.stream()
                .filter(record -> userId.equals(record.getUserId()))
                .count();
    }

    @Override
    public void clear(@NotNull final String userId) {
        removeAll(findAll(userId));
    }
}
