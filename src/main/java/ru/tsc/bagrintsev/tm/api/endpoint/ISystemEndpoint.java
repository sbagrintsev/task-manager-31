package ru.tsc.bagrintsev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.request.system.ServerAboutRequest;
import ru.tsc.bagrintsev.tm.dto.request.system.ServerVersionRequest;
import ru.tsc.bagrintsev.tm.dto.response.system.ServerAboutResponse;
import ru.tsc.bagrintsev.tm.dto.response.system.ServerVersionResponse;

public interface ISystemEndpoint {
    @NotNull
    ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request);

    @NotNull
    ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request);
}
