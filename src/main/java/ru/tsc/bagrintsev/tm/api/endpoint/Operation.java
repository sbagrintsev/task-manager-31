package ru.tsc.bagrintsev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.request.AbstractRequest;
import ru.tsc.bagrintsev.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    @NotNull
    RS execute(RQ request);

}
