package ru.tsc.bagrintsev.tm.client;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.endpoint.ISystemEndpoint;
import ru.tsc.bagrintsev.tm.dto.request.system.ServerAboutRequest;
import ru.tsc.bagrintsev.tm.dto.request.system.ServerVersionRequest;
import ru.tsc.bagrintsev.tm.dto.response.system.ServerAboutResponse;
import ru.tsc.bagrintsev.tm.dto.response.system.ServerVersionResponse;

public final class SystemEndpointClient extends AbstractEndpoint implements ISystemEndpoint {

    public static void main(String[] args) {
        @NotNull final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        @NotNull final ServerVersionResponse serverVersionResponse = client.getVersion(new ServerVersionRequest());
        System.out.println(serverVersionResponse.getVersion());
        @NotNull final ServerAboutResponse serverAboutResponse = client.getAbout(new ServerAboutRequest());
        System.out.println(serverAboutResponse.getEmail());
        System.out.println(serverAboutResponse.getName());
        client.disconnect();
    }

    @NotNull
    @Override
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        return (ServerAboutResponse) call(request);
    }

    @NotNull
    @Override
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        return (ServerVersionResponse) call(request);
    }

}
