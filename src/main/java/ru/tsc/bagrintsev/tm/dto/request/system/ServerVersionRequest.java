package ru.tsc.bagrintsev.tm.dto.request.system;

import lombok.Getter;
import lombok.Setter;
import ru.tsc.bagrintsev.tm.dto.request.AbstractRequest;

@Getter
@Setter
public final class ServerVersionRequest extends AbstractRequest {
}
