package ru.tsc.bagrintsev.tm.dto.request.system;

import lombok.Getter;
import lombok.Setter;
import ru.tsc.bagrintsev.tm.dto.request.AbstractRequest;

@Getter
@Setter
public final class ServerAboutRequest extends AbstractRequest {
}
