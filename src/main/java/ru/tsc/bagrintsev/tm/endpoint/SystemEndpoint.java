package ru.tsc.bagrintsev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.endpoint.ISystemEndpoint;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;
import ru.tsc.bagrintsev.tm.api.sevice.IServiceLocator;
import ru.tsc.bagrintsev.tm.dto.request.system.ServerAboutRequest;
import ru.tsc.bagrintsev.tm.dto.request.system.ServerVersionRequest;
import ru.tsc.bagrintsev.tm.dto.response.system.ServerAboutResponse;
import ru.tsc.bagrintsev.tm.dto.response.system.ServerVersionResponse;

public class SystemEndpoint implements ISystemEndpoint {

    @NotNull
    final IServiceLocator serviceLocator;

    public SystemEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @NotNull
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @Override
    @NotNull
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }
}
