package ru.tsc.bagrintsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.IProjectRepository;
import ru.tsc.bagrintsev.tm.api.sevice.IProjectService;
import ru.tsc.bagrintsev.tm.enumerated.Entity;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.model.Project;

import java.util.Date;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull final IProjectRepository repository) {
        super(repository);
    }

    @NotNull
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(EntityField.NAME, name);
        return repository.create(userId, name);
    }

    @NotNull
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(EntityField.NAME, name);
        check(EntityField.DESCRIPTION, description);
        return repository.create(userId, name, description);
    }

    @NotNull
    @Override
    public Project updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(index);
        check(EntityField.NAME, name);
        check(EntityField.DESCRIPTION, description);
        @NotNull final Project project = findOneByIndex(userId, index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(EntityField.ID, id);
        check(EntityField.NAME, name);
        check(EntityField.DESCRIPTION, description);
        @NotNull final Project project = findOneById(userId, id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    public Project changeProjectStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(index);
        check(Entity.STATUS, status);
        @NotNull final Project project = findOneByIndex(userId, index);
        project.setStatus(status);
        if (status.equals(Status.IN_PROGRESS)) {
            project.setDateStarted(new Date());
        } else if (status.equals(Status.COMPLETED)) {
            project.setDateFinished(new Date());
        } else if (status.equals(Status.NOT_STARTED)) {
            project.setDateStarted(null);
            project.setDateFinished(null);
        }
        return project;
    }

    @NotNull
    @Override
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(EntityField.ID, id);
        check(Entity.STATUS, status);
        @NotNull final Project project = findOneById(userId, id);
        project.setStatus(status);
        if (status.equals(Status.IN_PROGRESS)) {
            project.setDateStarted(new Date());
        } else if (status.equals(Status.COMPLETED)) {
            project.setDateFinished(new Date());
        } else if (status.equals(Status.NOT_STARTED)) {
            project.setDateStarted(null);
            project.setDateFinished(null);
        }
        return project;
    }

}
